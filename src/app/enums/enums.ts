export enum Locations {
    TAIPEI = '台北',
    KAOHSIUNG = '高雄',
    XIECHI = '協志',
    TAIYIN = '台銀'
}

export enum RoomStatuses {
    NO_MEETING_NO_PERSON = '空閒',
    NO_MEETING_HAS_PERSON = '有人亂入',
    HAS_MEETING_NO_PERSON = '有人放鴿子',
    HAS_MEETING_HAS_PERSON = '使用中'
}