import { Participant } from './Participant';

export class Meeting {
    subject: string;

    begin: Date;

    end: Date;

    estimatedNumbersOfParticipants: number;

    host: Participant;

    constructor(subject: string, begin: Date, end: Date,
        estimatedNumbersOfParticipants: number, host: Participant) {
        this.subject = subject;
        this.begin = begin;
        this.end = end;
        this.estimatedNumbersOfParticipants = estimatedNumbersOfParticipants;
        this.host = host;
    }
}
