import { Meeting } from './Meeting';
import { Locations, RoomStatuses } from '../enums/enums';

export class Room {
    name: string;

    location: Locations;

    capacity: number;

    currentNumbersOfParticipants: number;

    status: RoomStatuses;

    capture: string;

    currentMeeting: Meeting;

    nextMeeting: Meeting;

    constructor(name: string, location: Locations, capacity: number,
        currentNumbersOfParticipants: number, capture: string,
        currentMeeting: Meeting, nextMeeting: Meeting) {
        this.name = name;
        this.location = location;
        this.capacity = capacity;
        this.currentNumbersOfParticipants = currentNumbersOfParticipants;
        this.capture = capture;
        this.currentMeeting = currentMeeting;
        this.nextMeeting = nextMeeting;

        if (currentNumbersOfParticipants == 0) {
            if (!currentMeeting) {
                this.status = RoomStatuses.NO_MEETING_NO_PERSON;
            } else {
                this.status = RoomStatuses.HAS_MEETING_NO_PERSON;
            }
        } else {
            if (!currentMeeting) {
                this.status = RoomStatuses.NO_MEETING_HAS_PERSON
            } else {
                this.status = RoomStatuses.HAS_MEETING_HAS_PERSON;
            }
        }
    }
}
