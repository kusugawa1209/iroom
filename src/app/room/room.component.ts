import { Component, OnInit, Input } from '@angular/core';
import { Room } from '../models/Room';
import { RoomStatuses } from '../enums/enums';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})

export class RoomComponent implements OnInit {

  @Input() room: Room;

  constructor() { }

  ngOnInit() {
  }

  getBackgroundStyleClass(status: RoomStatuses) {
    if (status == RoomStatuses.HAS_MEETING_HAS_PERSON) {
      return 'alert-danger';
    } else {
      return 'alert-success';
    }
  }

}
