import { Component, OnInit } from '@angular/core';
import { Room } from '../models/Room';
import { Meeting } from '../models/Meeting';
import { Participant } from '../models/Participant';
import { Locations } from '../enums/enums';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  rooms: Room[];

  constructor() {
    this.initRooms();
  }

  ngOnInit() {
  }


  initRooms() {
    let rooms: Room[] = new Array();
    for (let i = 0; i < 10; i++) {
      let room = this.generateRoom(i);
      rooms.push(room);
    }

    rooms.sort(function (room1, room2) {
      if (room1.status != room2.status) {
        return room2.status.localeCompare(room1.status);
      } else if (room1.location != room2.location) {
        return room1.location.localeCompare(room2.location);
      } else {
        return room1.name.localeCompare(room2.name);
      }
    });

    this.rooms = rooms;
  }

  generateRoom(index: number) {
    let name = '測試會議室' + index;
    let location = this.generateLocation();
    let capture = this.generateCapture();
    let capacity = Math.floor(Math.random() * 27) + 5;
    let currentNumbersOfParticipants = Math.floor(Math.random() * capacity);
    let currentMeeting = this.generateMeeting(capacity);
    let nextMeeting = this.generateMeeting(capacity);

    return new Room(name, location, capacity, currentNumbersOfParticipants, capture, currentMeeting, nextMeeting);
  }

  generateLocation() {
    let random = Math.floor(Math.random() * 3);

    switch (random) {
      case 0: return Locations.TAIPEI;
      case 1: return Locations.KAOHSIUNG;
      case 2: return Locations.XIECHI;
      case 3: return Locations.TAIYIN;
      default: return Locations.TAIPEI;
    }
  }

  generateCapture() {
    let pictureIndex = Math.floor(Math.random() * 4) + 1;

    return 'assets/img/meeting-room-00' + pictureIndex + '.jpg';
  }

  generateMeeting(capacity: number) {
    let random = Math.floor(Math.random() * 2);
    console.log(random);

    if (random == 0) {
      let estimatedNumbersOfParticipants = Math.floor(Math.random() * (capacity - 1)) + 1;
      let host = new Participant('測試人員', '12345');

      return new Meeting('測試會議', new Date(), new Date(), estimatedNumbersOfParticipants, host);
    } else {
      return null;
    }

  }
}
